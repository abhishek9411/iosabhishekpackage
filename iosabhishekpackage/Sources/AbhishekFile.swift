//
//  File.swift
//  
//
//  Created by Abhishek Singh on 01/07/23.
//

import Foundation
import SwiftUI

@available(ios 13.0, *)
public struct AbhishekView : View {
   public init() {}
    
    @available(macOS 10.15.0, *)
    public var body: some View{
        VStack{
            Text("this is my first view").padding()
            Text("this is my second view")

        }
    }
}
